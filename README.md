# [gitara.info](https://gitara.info) source codes


<br/>

### Run gitara.info on localhost

    # vi /etc/systemd/system/gitara.info.service

Insert code from gitara.info.service
    
    # systemctl enable gitara.info.service
    # systemctl start  gitara.info.service
    # systemctl status gitara.info.service


http://localhost:4020
